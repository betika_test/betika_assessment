import { test, expect } from '@playwright/test';
import { navigateHelper } from '../helpers/navigateHelper';
import { betslip } from '../pages/betslip';
import { login } from '../pages/login';
import { soccer } from '../pages/soccer';

test.describe("Betslip", () => {
    const betikaUrl = "https://www.betika.com/en-ke/";
    const betikaSoccerUrl = "https://www.betika.com/en-ke/s/soccer";
    const betikaPlaceboUrl = "https://www.betika.com/en-ke/betslip";
    const username = "0743551248";//this has to come from some db at some point
    const password = "1234";//this has to come from some db at some point
    const odds = ['1.55','2.18', '1.95'];//this need to come from an odds db or api
    const foundBetslipCode = "ey38m7"//value stored to be stored on a json file
    const notfoundBetslipCode = "ey3888"//value stored to be stored on a json file
    const betAmount = "10";//bet amount

    test.beforeEach(async ({page, baseURL}) => {
        //navigate to betika and login
        const navigate = new navigateHelper(page, baseURL);
        await navigate.gotourl();
    });

    test("validate placebet", async ({ page }) => {
        const loginPage = new login(page);
        await loginPage.login(username, password);

        const betslipPage = new betslip(page);
        const soccerPage = new soccer(page);
        //validating url has betika etc. then also checking navigation controls
        await expect(page).toHaveURL(betikaUrl);
        await expect(soccerPage.navHome).toBeVisible();
        await expect(soccerPage.soccer).toBeVisible();

        //navigating to pages
        await soccerPage.navigateToHome();
        await soccerPage.navigateToSoccer();

        //validating page url and matches available
        await expect(page).toHaveURL(betikaSoccerUrl);
        await expect(soccerPage.matches).toBeVisible();

        //filtering matches
        await soccerPage.navigateToUpcomingMatches();
        await soccerPage.clickAllUpcomingMatches();
        await soccerPage.clickTomorrowUpcomingMatches();

        //validating elements are visible
        await expect(soccerPage.advertisement).toBeVisible();
        await expect(betslipPage.betslipDescritpon).toBeVisible();
        await expect(betslipPage.loadBetslipBtn).toBeVisible();

        //selecting odds
        await soccerPage.selectMatchOdds(odds);

        //validating elements are visible
        await expect(betslipPage.mainBetslip).toBeVisible();
        await expect(betslipPage.mainBetslipNav).toBeVisible();
        await expect(betslipPage.slips).toBeVisible();
        await expect(betslipPage.KeepSlipBtn).toBeVisible();
        await expect(betslipPage.roundedSlips).toBeVisible();

        //validating elements are visible
        for (let i = 0; i < betslipPage.betslipValues.length; i++) {
            await expect(betslipPage.betslipValues[i]).toBeVisible();
        }

        //still validting elements visibility
        await expect(betslipPage.amount).toBeVisible();
        await expect(betslipPage.shareBtn).toBeVisible();
        await expect(betslipPage.placeBetBtn).toBeVisible();

        //finally placing a bet with the specified amount
        await betslipPage.placeBet(betAmount);

        //validting notification for success if showing
        await expect(betslipPage.betsuccessNotification).toBeVisible();
    });

    test("check Remove all slips", async ({ page }) => {
        const betslipPage = new betslip(page);
        const soccerPage = new soccer(page);
        //navigating to pages
        await soccerPage.navigateToHome();
        await soccerPage.navigateToSoccer();

        //filtering matches
        await soccerPage.navigateToUpcomingMatches();
        await soccerPage.clickAllUpcomingMatches();
        await soccerPage.clickTomorrowUpcomingMatches();

        //slecting odds
        await soccerPage.selectMatchOdds(odds);

        //checking if remove all is there
        await expect(betslipPage.betslipRemoveAllBtn).toBeVisible();
        await betslipPage.removeAllSlips();

        //for closing the pop when deleting all slips
        async function handleDialogBox() {
            page.once("dialog", async (dialog) => {
            await dialog.accept();
            });
        }
        await handleDialogBox();

        // Wait for the dialog to be accepted
        await page.waitForSelector("#dialog-message", { state: "hidden" });
        await expect(betslipPage.betslipDescritpon).toBeVisible();
    });

    test("check betslip navigation", async ({ page }) => {
        const betslipPage = new betslip(page);
        const soccerPage = new soccer(page);

        //navigating to pages
        await soccerPage.navigateToHome();
        await soccerPage.navigateToSoccer();

        //validating navigation if elements supposed to be visible are there and validating if expectec elements are there.
        for (let i = 0; i < betslipPage.mainBetslipNavOptions.length; i++) {
            await expect(betslipPage.mainBetslipNavOptions[i]).toBeVisible();
            await betslipPage.clickOnElement(betslipPage.mainBetslipNavOptions[i]);
        }

    });

    test("check Remove Slip", async ({ page }) => {
        const betslipPage = new betslip(page);
        const soccerPage = new soccer(page);
        const index = 1;

        //navigating to pages
        await soccerPage.navigateToHome();
        await soccerPage.navigateToSoccer();

        //filtering matches
        await soccerPage.navigateToUpcomingMatches();
        await soccerPage.clickAllUpcomingMatches();
        await soccerPage.clickTomorrowUpcomingMatches();

        //selecting odds
        await soccerPage.selectMatchOdds(odds);

        //counting and removing slip
        const initialSlipCount = await betslipPage.SlipRemoveBtn.length;
        for (let i = 0; i < betslipPage.SlipRemoveBtn.length; i++) {
            await expect(betslipPage.SlipRemoveBtn[i]).toBeVisible();
        }
        //removing slips
        await betslipPage.removeSpecificBetSlip(index);
        //comparing and validating what is expected
        if(initialSlipCount == 1){
            await expect(betslipPage.betslipDescritpon).toBeVisible();
        }else{
            for (let i = 0; i < betslipPage.SlipRemoveBtn.length; i++) {
                await expect(betslipPage.SlipRemoveBtn[i]).toBeVisible();
            }
        }
    });

    test("check Related bets", async ({ page }) => {
        const loginPage = new login(page);
        await loginPage.login(username, password);

        const betslipPage = new betslip(page);
        const soccerPage = new soccer(page);

        //navigating to the pages
        await soccerPage.navigateToHome();
        await soccerPage.navigateToSoccer();

        //filtering to tomorrows upcoming matches
        await soccerPage.navigateToUpcomingMatches();
        await soccerPage.clickAllUpcomingMatches();
        await soccerPage.clickTomorrowUpcomingMatches();

        //selecting match odds
        await soccerPage.selectMatchOdds(odds);

        //validating ecpected fields or controls
        await expect(betslipPage.relatedBets).toBeVisible();
        await expect(betslipPage.relatedBetsTitle).toBeVisible();
        await expect(betslipPage.relatedBetsCarousel).toBeVisible();
        for (let i = 0; i < betslipPage.relatedBetsCarouselPaginationBtn.length; i++) {
            await expect(betslipPage.relatedBetsCarouselRecommendation[i]).toBeVisible();
            await betslipPage.clickOnElement(betslipPage.relatedBetsCarouselPaginationBtn[i]);
        }
    });

    test("load slip - Not found", async ({ page }) => {
        const betslipPage = new betslip(page);
        const soccerPage = new soccer(page);

        //naviating to the pages
        await soccerPage.navigateToHome();
        await soccerPage.navigateToSoccer();

        //expected fields to be visible
        await expect(betslipPage.betslipDescritpon).toBeVisible();
        await expect(betslipPage.betslipTxt).toBeVisible();
        await expect(betslipPage.loadBetslipBtn).toBeVisible();

        //loading betslip code and expecting not found value to show
        await betslipPage.loadBetslip(notfoundBetslipCode);
        await expect(betslipPage.betslipNotFoundTxt).toBeVisible();
        expect(await betslipPage.betslipNotFoundTxt.textContent()).not.toBeNull();
    });

    test("load slip - found", async ({ page }) => {
        const betslipPage = new betslip(page);
        const soccerPage = new soccer(page);

        //navigating to pages
        await soccerPage.navigateToHome();
        await soccerPage.navigateToSoccer();

        //expected fields to be visible
        await expect(betslipPage.betslipDescritpon).toBeVisible();
        await expect(betslipPage.betslipTxt).toBeVisible();
        await expect(betslipPage.loadBetslipBtn).toBeVisible();

        //loading of the betslip
        await betslipPage.loadBetslip(foundBetslipCode);
        //expecting to see slips on the page
        await expect(betslipPage.roundedSlips).toBeVisible();

        //expecting to see slip amount or values i.e total odds etc.
        for (let i = 0; i < betslipPage.betslipValues.length; i++) {
            await expect(betslipPage.betslipValues[i]).toBeVisible();
        }

        //expected fields or controls to be visible
        await expect(betslipPage.amount).toBeVisible();
        await expect(betslipPage.shareBtn).toBeVisible();
        await expect(betslipPage.placeBetBtn).toBeVisible();
    });

    test("Check shared betslip", async ({ page }) => {

        const betslipPage = new betslip(page);
        const soccerPage = new soccer(page);
        //navigating to the home then soccer page
        await soccerPage.navigateToHome();
        await soccerPage.navigateToSoccer();

        //validating that the expected ui fields are visible if not it will fail
        await expect(betslipPage.betslipDescritpon).toBeVisible();
        await expect(betslipPage.betslipTxt).toBeVisible();
        await expect(betslipPage.loadBetslipBtn).toBeVisible();

        //filtering the matches to only show tomorrow upcoming matches
        await soccerPage.navigateToUpcomingMatches();
        await soccerPage.clickAllUpcomingMatches();
        await soccerPage.clickTomorrowUpcomingMatches();

        //selecting odds stored on the odds variable or coming from an api etc.
        await soccerPage.selectMatchOdds(odds);
        
        //expecting that share btn to be visible then proceed upon clicking on it.
        await expect(betslipPage.shareBtn).toBeVisible();
        await betslipPage.shareBetslip();

        //once the share modal is visible the extract url on the textbox and get the code at the end
        //the code can be stored on the DB or file for testing the load functionality works
        const SharedBetSlipUrl = await betslipPage.shareModalLnkTxt.textContent();
        const index = SharedBetSlipUrl.lastIndexOf("/");
        //code to be stored each time on a json file to be used for other tests
        const betslipCode = SharedBetSlipUrl.substring(index + 1);

        //closing the modal then going to the shared url
        await betslipPage.closeShareModal();
        await betslipPage.gotoUrl(SharedBetSlipUrl);
        //validating page url
        //await expect(page).toHaveURL(betikaPlaceboUrl).waitFor();
        //expecting to see slips on the page
        await expect(betslipPage.roundedSlips).toBeVisible();

        //expecting to see slip amount or values i.e total odds etc.
        for (let i = 0; i < betslipPage.betslipValues.length; i++) {
            await expect(betslipPage.betslipValues[i]).toBeVisible();
        }

        //expected fields or controls to be visible
        await expect(betslipPage.amount).toBeVisible();
        await expect(betslipPage.shareBtn).toBeVisible();
        await expect(betslipPage.placeBetBtn).toBeVisible();
    });

    test.skip("check share slip", async ({ page }) => {
        const betslipPage = new betslip(page);
        const soccerPage = new soccer(page);

        //navigating to pages
        await soccerPage.navigateToHome();
        await soccerPage.navigateToSoccer();

        //match filtering
        await soccerPage.navigateToUpcomingMatches();
        await soccerPage.clickAllUpcomingMatches();
        await soccerPage.clickTomorrowUpcomingMatches();

        //selecting odds
        await soccerPage.selectMatchOdds(odds);

        //expected fields to be visble then share slip
        await expect(betslipPage.shareBtn).toBeVisible();
        await betslipPage.shareBetslip();

        //expected fields for the share slip modal
        await expect(betslipPage.shareModal).toBeVisible();
        await expect(betslipPage.shareModalCloseBtn).toBeVisible();
        await expect(betslipPage.shareModalOptions).toBeVisible();
        await expect(betslipPage.shareModalLnkTxt).toBeVisible();
        expect(await betslipPage.shareModalLnkTxt.textContent()).not.toBeNull();
        await expect(betslipPage.shareModalCopyLnkBtn).toBeVisible();
        await expect(betslipPage.shareModalCancelBtn).toBeVisible();

        //validating copy link button works
        await betslipPage.clickCopyLink();
        //checking for the success notification
        await expect(betslipPage.betsuccessNotification).toBeVisible();

        //closing the modal
        await betslipPage.closeShareModal();
    });
});