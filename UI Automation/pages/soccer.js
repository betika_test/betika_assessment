export class soccer {
    constructor(page){
        this.page = page;
        this.navHome = page.locator('a').filter({ hasText: 'Home' });
        this.soccer = page.getByText('Soccer', { exact: true });
        this.matches = page.locator('div.matches__container');
		this.upcomingMatches = page.getByRole('button', { name: 'Upcoming' });
		this.upcomingAllMatches = page.getByRole('button', { name: 'All' });
		this.tomorrowUpcomingMatches = page.getByRole('button', { name: 'Tomorrow' });
        this.highlights = page.locator('button').filter({ hasText: 'Highlights' });
        this.advertisement = page.locator('div.VueCarousel');
        this.matchesToBet = page.locator('div.prebet-match');//list
        this.matchHeader = page.locator('div.prebet-match > div.time');//list header
        this.matchTitle = page.locator('div.prebet-match > div.time > div > span > span');//list title
        this.matchPreBetOddsSection = page.locator('div.prebet-match__odd-market__container');//list body
        this.homeTeam = page.locator('span.prebet-match__teams__home');//list
        this.awayTeam = page.locator('div.prebet-match__teams > span:nth-child(2)');//list
        this.odds = page.locator('div.prebet-match__odds__container');//list
        this.specificOdd = page.locator('div:nth-child(7) > div.prebet-match__odd-market__container > div.prebet-match__odds__container > div > button.prebet-match__odd.selected > span');//list
        this.oddBtn = page.locator('button');
        this.selector = page.locator('div.prebet-match__odds__container > div > button');
        this.columnOneOddBtn = page.locator('div.prebet-match__odds__container > div > button:nth-child(1)');
    }

    async navigateToHome(){
        await this.navHome.click();
    }

    async navigateToSoccer(){
        await this.soccer.click();
    }

    async navigateToUpcomingMatches(){
        await this.upcomingMatches.click();
    }

    async clickAllUpcomingMatches(){
        await this.upcomingAllMatches.click();
    }

    async clickTomorrowUpcomingMatches(){
        await this.tomorrowUpcomingMatches.click();
    }

    async navigateToHighlights(){
        await this.highlights.click();
    }

    async selectMatchOdds(oddsToSelect){
        for (let i = 0; i < oddsToSelect.length; i++) {
            await this.oddBtn.filter({ hasText: oddsToSelect[i].toString() }).scrollIntoViewIfNeeded();
            await this.oddBtn.filter({ hasText: oddsToSelect[i].toString() }).click();
        }
    }

    async selectRandomMatchOdds(teams){
        for (let i = 0; i < teams; i++) {
            //await this.columnOneOddBtn[i].scrollIntoViewIfNeeded();
            await this.columnOneOddBtn[i].click();
        }
    }
}