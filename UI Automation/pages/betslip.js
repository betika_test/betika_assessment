export class betslip {
    constructor(page){
        this.page = page;
		this.mainBetslip = page.locator('div.main-betslip');
        this.mainBetslipNav = page.locator('div.main-betslip__nav');
        this.mainBetslipNavOptions = page.locator('div.main-betslip__nav > div');
        this.slips = page.locator('div#slips');
        this.betslipDescritpon = page.locator('p.load-betslip__desc');
        this.betslipTxt = page.locator('input.load-betslip__sharing-code__input');
        this.betslipNotFoundTxt = page.locator('span.error');
        this.loadBetslipBtn = page.getByRole('button', { name: 'Load Betslip' });
        this.KeepSlipBtn = page.locator('div.input-group-inline__checkbox');
        this.betslipRemoveAllBtn = page.locator('span').filter({ hasText: 'Remove All' });
        this.roundedSlips = page.locator('div.rounded-card');
        this.SlipRemoveBtn = page.locator('div.rounded-card > div.stacked > button');
        this.SlipDetails = page.locator('div.rounded-card > div.stacked > div.stacked__details');//list
        this.SlipTeamTitles = page.locator('a.stacked__link');//list
        this.bestSlipDetails = page.locator('div.betslip__details');//list
        this.betslipValues = page.locator('span.betslip__details__row__value');
        this.amount = page.locator('div.betslip__details__row--amount > div > div > div > input');
        this.shareBtn = page.locator('span').filter({ hasText: 'Share' });
        this.placeBetBtn = page.locator('span').filter({ hasText: 'Place Bet' });
        this.betsuccessNotification = page.locator('#notifications-root > div.notification.show.success');

        this.relatedBets = page.locator('div.bet-recommendations');
        this.relatedBetsTitle = page.locator('div.bet-recommendations > div.bet-recommendations__header > p');
        this.relatedBetsCarousel = page.locator('div.bet-recommendations > div.VueCarousel');
        this.relatedBetsCarouselRecommendation = page.locator('div.bet-recommendations > div.VueCarousel > div > div.VueCarousel-inner > div');
        this.relatedBetsCarouselPaginationBtn = page.locator('div.VueCarousel-dot-container > button');

        this.shareModal = page.locator('div.share-betslip__open');
        this.shareModalCloseBtn = page.locator('button.modal__x');
        this.shareModalOptions = page.locator('div.modal__container-links.share-betslip__socials');
        this.shareModalLnkTxt = page.locator('div.input__container.share-betslip__input > input');
        this.shareModalCopyLnkBtn = page.locator('button').filter({ hasText: 'Copy Link' });
        this.shareModalCancelBtn = page.locator('button').filter({ hasText: 'Cancel' });
    }

    async removeAllSlips(){
        await this.betslipRemoveAllBtn.click();
    }
    
    async keepSlip(){
        await this.KeepSlipBtn.click();
    }

    async enterAmount(value){
        await this.amount.fill(value);
    }

    async clickOnElement(locator){
        await locator.click();
    }

    async removeSpecificBetSlip(index){
        await this.page.locator('div:nth-child('+index+') > .stacked__remove').click();
    }
    async numberOfSlips(){
        return await this.SlipRemoveBtn.length;
    }

    async shareBetslip(){
        await this.shareBtn.click();
    }

    async clickPlaceBetBtn(){
        await this.placeBetBtn.click();
    }

    async clickloadBetslipBtn(){
        await this.loadBetslipBtn.click();
    }

    async enterSlipCode(code){
        await this.betslipTxt.clear();
        await this.betslipTxt.fill(code);
    }

    async loadBetslip(code){
        await this.enterSlipCode(code);
        await this.clickloadBetslipBtn();
    }

    async placeBet(amount){
        await this.keepSlip();
        await this.enterAmount(amount);
        await this.clickPlaceBetBtn();
    }

    async closeShareModal(){
        await this.shareModalCloseBtn.click();
    }

    async clickCopyLink(){
        await this.shareModalCopyLnkBtn.click();
    }

    async gotoUrl(url){
        await this.page.goto(url, {waitUntil: 'load', timeout: 900000});
    }
}