export class login {
    constructor(page){
        this.page = page;
        this.loginNavBtn = page.locator('a').filter({ hasText: 'Login' });
		this.phoneNumberTxt = page.getByRole('textbox', { name: 'e.g. 0712 234567' })
		this.passwordTxt = page.locator('div.session__form__password__container > div > input');
        this.loginBtn = page.getByRole('button', { name: 'Login' });
    }

    async enterPhoneNumner(phone){
        await this.phoneNumberTxt.clear();
        await this.phoneNumberTxt.fill(phone);
    }

    async enterPassword(password){
        await this.passwordTxt.clear();
        await this.passwordTxt.fill(password);
    }

    async clickNavLoginBtn()
    {
        await this.loginNavBtn.click();
    }

    async clickLoginBtn()
    {
        await this.loginBtn.click();
    }

    async login(phone, password)
    {
       await this.clickNavLoginBtn();
       await this.enterPhoneNumner(phone);
       await this.enterPassword(password);
       await this.clickLoginBtn();
    }
}